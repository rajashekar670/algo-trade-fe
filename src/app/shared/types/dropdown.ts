export class DropDown {
  constructor(key: string, value: string, selected?: boolean ) {
    (this.key = key), (this.value = value), (this.selected = selected);
  }
  key!: string;
  value!: string;
  selected?: boolean;
}
