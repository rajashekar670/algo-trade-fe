import { CommonModule } from '@angular/common';
import { Component, Input } from '@angular/core';
import { FormControl, ReactiveFormsModule } from '@angular/forms';

@Component({
  selector: 'app-field-error',
  standalone: true,
  imports: [ReactiveFormsModule, CommonModule, FieldErrorComponent],
  templateUrl: './field-error.component.html',
  styleUrl: './field-error.component.css'
})
export class FieldErrorComponent {
  @Input()
  public formField!: FormControl;
  constructor() { }

}
