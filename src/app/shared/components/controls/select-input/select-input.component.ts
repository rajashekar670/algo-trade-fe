import { Component, Input, Output, forwardRef , EventEmitter} from '@angular/core';
import { ControlValueAccessor, FormControl, FormGroup, NG_VALUE_ACCESSOR, ReactiveFormsModule } from '@angular/forms';
import { FieldErrorComponent } from '../field-error/field-error.component';
import { CommonModule } from '@angular/common';

@Component({
  selector: '.app-select-input',
  standalone: true, 
  imports: [ReactiveFormsModule, FieldErrorComponent, CommonModule],
  templateUrl: './select-input.component.html',
  styleUrl: './select-input.component.css',
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => SelectInputComponent),
      multi: true,
    },
  ],
})
export class SelectInputComponent implements ControlValueAccessor {

  constructor() { }

  @Input()
  public parentForm!: FormGroup;

  @Input()
  public fieldName!: string;

  @Input()
  public label!: string;

  @Input()
  public type!: string;

  @Input()
  public items!: any;

  @Output() changeEvent = new EventEmitter<string>();

  public value: string ='' ;

  public changed!: (value: string) => void;

  public touched!: (() => void);

  public isDisabled: boolean = false;

  get formField() {
    return this.parentForm?.get(this.fieldName) as FormControl
  }

  onChange(event:any){
    this.changed(event.target.value);
    this.changeEvent.emit(event.target.value);
  }

  writeValue(value: string): void {
    this.value = value;
  }
  registerOnChange(fn: any): void {
    this.changed = fn;
  }
  registerOnTouched(fn: any): void {
    this.touched = fn;
  }
  setDisabledState?(isDisabled: boolean): void {
    this.isDisabled = isDisabled;
  }
}
