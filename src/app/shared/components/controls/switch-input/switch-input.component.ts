import { Component, Input, forwardRef } from '@angular/core';
import { FormControl, FormGroup, NG_VALUE_ACCESSOR } from '@angular/forms';
import { TextInputComponent } from '../text-input/text-input.component';

@Component({
  selector: '.app-switch-input',
  standalone: true,
  imports: [],
  templateUrl: './switch-input.component.html',
  styleUrl: './switch-input.component.css',
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => SwitchInputComponent),
      multi: true,
    },
  ],
})
export class SwitchInputComponent {
  constructor() {}

  @Input()
  public parentForm!: FormGroup;

  @Input()
  public fieldName!: string;

  @Input()
  public label!: string;

  public value: boolean = false ;

  public changed!: (value: string) => void;

  public touched!: (() => void);

  public isDisabled: boolean = false;

  get formField() {
    return this.parentForm?.get(this.fieldName) as FormControl
  }

  onChange(event:any){
    this.changed(event.target.checked)
  }

  writeValue(value: boolean): void {
    this.value = value;
  }
  registerOnChange(fn: any): void {
    this.changed = fn;
  }
  registerOnTouched(fn: any): void {
    this.touched = fn;
  }
  setDisabledState?(isDisabled: boolean): void {
    this.isDisabled = isDisabled;
  }
}
