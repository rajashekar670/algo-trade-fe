import { Routes } from '@angular/router';
import { ShortStrangleStrategyFormComponent } from './modules/pages/strategies/strategy-form/short-strangle-strategy-form/short-strangle-strategy-form.component';
import { StrategyListComponent } from './modules/pages/strategies/strategy-list/strategy-list.component';
import { ShortStrangleStrategyDetailComponent } from './modules/pages/strategies/strategy-detail/short-strangle-strategy-detail/short-strangle-strategy-detail.component';
import { ShortStraddleStrategyFormComponent } from './modules/pages/strategies/strategy-form/short-straddle-strategy-form/short-straddle-strategy-form.component';
import { ShortStraddleStrategyDetailComponent } from './modules/pages/strategies/strategy-detail/short-straddle-strategy-detail/short-straddle-strategy-detail.component';

export const routes: Routes = [
    {
        path:'strategies/create/short-straddle-strategy', component:ShortStraddleStrategyFormComponent
    },
    {
        path:'strategies/create/short-strangle-strategy', component:ShortStrangleStrategyFormComponent
    },
    {
        path:'strategies', component:StrategyListComponent
    },
    {
        path:'strategies/Short Strangle/:id', component:ShortStrangleStrategyDetailComponent
    },
    {
        path:'strategies/Short Straddle/:id', component:ShortStraddleStrategyDetailComponent
    }
];
