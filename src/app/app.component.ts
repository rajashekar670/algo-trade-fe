import { Component, OnInit } from '@angular/core';
import { RouterModule, RouterOutlet } from '@angular/router';
import { HeaderComponent } from './core/components/header/header.component';
import { SidebarComponent } from './core/components/sidebar/sidebar.component';
import { HttpClientModule, withFetch } from '@angular/common/http';
import { CommonModule } from '@angular/common';
import { LoginComponent } from './core/components/login/login.component';

@Component({
  selector: 'app-root',
  standalone: true,
  imports: [
    RouterOutlet,
    HeaderComponent,
    SidebarComponent,
    HttpClientModule,
    CommonModule,
    RouterModule,
    LoginComponent,
  ],
  providers:[],
  templateUrl: './app.component.html',
  styleUrl: './app.component.css',
})
export class AppComponent implements OnInit {
  ngOnInit(): void {
  }
  title = 'Algo Trade';
}
