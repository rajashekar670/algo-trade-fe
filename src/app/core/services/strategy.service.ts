import {
  HttpClient,
  HttpErrorResponse,
  HttpParams,
} from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { catchError, retry } from 'rxjs/operators';
import {
  URL_START_STRATEGY,
  URL_STRATEGIES,
  URL_STRATEGY,
  constants,
} from '../constants/constants';
import { Strategy } from '../types/strategy';
import { PagedStrategyData } from '../types/data/paged-strategy-data';

@Injectable({
  providedIn: 'root',
})
export class StrategyService {
  constructor(private http: HttpClient) {}

  createStrategy(form: any): Observable<unknown> {
    const user = form.user;
    const id = form.id;
    return this.http
      .post<any>(URL_STRATEGY(user, id), form, {
        observe: 'response',
      })
      .pipe(catchError(this.handleError));
  }

  getStrategies(status: string): Observable<PagedStrategyData> {
    const options = status
      ? { params: new HttpParams().set('status', status) }
      : {};
    return this.http
      .get<any>(URL_STRATEGIES('rajashekar670@gmail.com'), options)
      .pipe(catchError(this.handleError));
  }

  startStrategy(id: string): Observable<any> {
    return this.http
      .get<any>(URL_START_STRATEGY('rajashekar670@gmail.com', id))
      .pipe(catchError(this.handleError));
  }

  getStrategy(id: string): Observable<any> {
    return this.http
      .get<any>('/api/users/rajashekar670@gmail.com/strategies/' + id)
      .pipe(catchError(this.handleError));
  }

  deleteStrategy(id: string, user: string): void {
    this.http
      .delete<void>(URL_STRATEGY(user, id))
      .pipe(catchError(this.handleError)).subscribe();
  }

  handleError(error: HttpErrorResponse) {
    alert(JSON.stringify(error.error));
    return throwError(() => new Error('Error occured'));
  }
}
