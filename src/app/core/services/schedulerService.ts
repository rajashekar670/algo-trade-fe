import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { URL_DELETE_JOB } from '../constants/constants';
import { catchError, throwError } from 'rxjs';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class SchedulerService {
  constructor(private http: HttpClient) {}

  deleteJob(id: string): void {
    this.http
      .delete<any>(URL_DELETE_JOB(id))
      .pipe(catchError(this.handleError)).subscribe();
  }

  handleError(error: HttpErrorResponse) {
    alert(JSON.stringify(error.message));
    return throwError(() => new Error('Error occured'));
  }
}
