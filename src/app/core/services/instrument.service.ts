import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { constants } from '../constants/constants';
import { Observable, catchError, throwError } from 'rxjs';
import { InstrumentData } from '../types/data/instrument-data';

@Injectable({
  providedIn: 'root',
})
export class InstrumentService {

  constructor(private http: HttpClient) { }

  getAllInstruments() : Observable<InstrumentData[]> {
      return this.http.get<InstrumentData[]>(constants.ALL_INSTRUMENTS_URL).pipe(catchError(this.handleError));
  }

  handleError(error: HttpErrorResponse) {
    alert(error);
    return throwError(() => new Error('Error occured'));
  }
}
