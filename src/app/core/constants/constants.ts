export const constants = {
  SEGMENTS_LIST: [
    { key: 'Options', value: 'OPTIONS' },
    { key: 'Equity', value: 'EQUITY' },
  ],
  STRATEGIES_LIST: [{ key: 'Moving Average', value: 'MOVING_AVERAGE' }],
  STATUS_LIST: [
    { key: 'New', value: 'NEW' },
    { key: 'Running', value: 'RUNNING' },
    { key: 'Completed', value: 'COMPLETED' },
    { key: 'Error', value: 'ERROR' },
  ],
  EXPIRY_LIST: [
    { key: 'Weekly', value: 'WEEKLY' },
    { key: 'Monthly', value: 'MONTHLY' },
    { key: 'Not Applicable', value: 'NA' },
  ],
  OPTION_TYPE_LIST: [
    { key: 'Option Buying', value: 'BUY' },
    { key: 'Option Selling', value: 'SELL' },
    { key: 'Not Applicable', value: 'NA' },
  ],
  STRANGLE_ENTRY_TYPE: [
    { key: 'Premium', value: 'PREMIUM' },
    { key: 'Strike Price', value: 'STRIKEPRICE' },
  ],
  STRADDLE_ADJUSTMENT_TYPE: [
    { key: 'Stop loss', value: 'STOPLOSS' },
    { key: 'Straddle', value: 'STRADDLE' },
  ],
  ALL_INSTRUMENTS_URL : '/api/instruments',
  URL_STRATEGY_SHORT_STRANGLE : '/short-strangle',
  URL_STRATEGY_START_ACTION : '/api/users/{user}/strategies/{strategy}/actions/start',
  URL_GET_ALL_STRATEGIES : '/api/users/{user}/strategies',
};

export const URL_STRATEGY = (user:string, id:string) => `/api/users/${user}/strategies/${id}`;
export const URL_STRATEGIES = (user:string) => `/api/users/${user}/strategies`;
export const URL_START_STRATEGY = (user:string, id:string) => `/api/users/${user}/strategies/${id}/actions/start`;
export const URL_DELETE_JOB = (id:string) => `/api/scheduler/jobs/${id}`;