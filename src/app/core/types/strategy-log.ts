export class StrategyLog {
  id!: number;
  type?: string;
  message!: string;
  createdTime!: string;
}
