import { StrategyData } from "./strategy-data";

export interface PagedStrategyData {
    currentPage:number,
    totalPages:number, 
    totalItems:number,
    items:[StrategyData]
}