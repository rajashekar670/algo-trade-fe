export class StrategyEntryData {
    id!: number;
    side!:string;
    quantity!:number;
    entry!:number;
    exit!:number;
    status!:string;
    ltp!:number;
    orderId!:string;
    optionType?:string;
    tradingSymbol?:string;
    createdTime!:string;
    modifiedTime!:string;
    strikePrice?:number;
    pl!:number;
}