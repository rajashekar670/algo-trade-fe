import { StrategyLog } from '../strategy-log';
import { BrokerData } from './broker-data';
import { InstrumentData } from './instrument-data';
import { StrategyEntryData } from './strategy-entry-data';
import { UserData } from './user-data';

export class StrategyData {
  user!: UserData;
  id!: string;
  instrument!: InstrumentData;
  quantity!: number;
  segment!: string;
  strategy!: string;
  status!: string;
  frequency!: number;
  label?: string;
  paperTrade!: boolean;
  pl: number = 0;
  broker!: BrokerData;
  expiry!: string;
  expiryDate!: string;
  optionType!: string;
  additionalATMPoints!: number;
  startTime!: string;
  endTime!: string;
  target!: number;
  logsEnabled!: boolean;
  entries?:StrategyEntryData[];
  logs?:StrategyLog[];
  jobStatus:boolean = false;
}
