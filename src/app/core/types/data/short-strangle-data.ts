import { StrategyData } from './strategy-data';

export class ShortStrangleData extends StrategyData {
  entrySelectionType!: string;
  callStrikePrice!: number;
  putStrikePrice!: number;
  callPremium!: number;
  putPremium!: number;
  dailyStartTime!: string;
  dailyEndTime!: string;
  eodMaxDiffPercentage!: number;
  adjustmentMaxDiffPercentage!: number;
  minAdjustmentPercentage!: number;
  straddleAdjustmentType!: string;
  stoplossPremium!: number;
  stoplossUpdateDiffPercentage!: number;
  stoplossUpdatePercentage!: number;
  totalPremium!: number;
  callStopLossStrikePrice!: number;
  putStopLossStrikePrice!: number;
  straddleAdjustmentPercentage!: number;
  targetPercentage!: number;
}
