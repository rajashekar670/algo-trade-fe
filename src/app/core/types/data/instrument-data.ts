import { ExpiryDateData } from "./expiry-date-data";

export class InstrumentData {
  name!: string;
  expiryDates!: ExpiryDateData[];
}
