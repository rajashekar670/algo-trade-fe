import { StrategyData } from './strategy-data';

export class ShortStraddleData extends StrategyData {
  adjustmentType!: string;
  adjustmentPercentage!: number;
  callStopLossStrikePrice!: number;
  putStopLossStrikePrice!: number;
  callStopLossPercentage!: number;
  putStopLossPercentage!: number;
  callStopLossPremium!: number;
  putStopLossPremium!: number;
  bufferStopLossForStrikePrice!: number;
  maxPL!: number;
  maxDiffAdjustmentPercentage!: number;
  targetPercentage!: number;
  dailyStartTime!: string;
}
