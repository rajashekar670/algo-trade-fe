import { InstrumentData } from "./data/instrument-data";

export class Strategy {
    user?: string;
    id!: string;
    instrument?: InstrumentData;
    quantity?: number;
    segment?: string;
    strategy?: string;
    status?: string;
    frequency?: number;
    label?: string;
    paperTrade?: boolean;
    pl: number = 0;
    broker?: string;
    expiry?: string;
    expiryDate?: string;
    optionType?: string;
    additionalATMPoints?: number;
    startTime?: string;
    endTime?: string;
    target?: number;
    logsEnabled?: boolean;
}