import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ShortStraddleStrategyFormComponent } from './short-straddle-strategy-form.component';

describe('ShortStraddleStrategyFormComponent', () => {
  let component: ShortStraddleStrategyFormComponent;
  let fixture: ComponentFixture<ShortStraddleStrategyFormComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [ShortStraddleStrategyFormComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(ShortStraddleStrategyFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
