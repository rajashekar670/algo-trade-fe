import { Component } from '@angular/core';
import {
  FormControl,
  FormGroup,
  ReactiveFormsModule,
  Validators,
} from '@angular/forms';
import { TextInputComponent } from '../../../../../shared/components/controls/text-input/text-input.component';
import { SelectInputComponent } from '../../../../../shared/components/controls/select-input/select-input.component';
import { SwitchInputComponent } from '../../../../../shared/components/controls/switch-input/switch-input.component';
import { InstrumentService } from '../../../../../core/services/instrument.service';
import { StrategyService } from '../../../../../core/services/strategy.service';
import { DropDown } from '../../../../../shared/types/dropdown';
import { constants } from '../../../../../core/constants/constants';
import { InstrumentData } from '../../../../../core/types/data/instrument-data';

@Component({
  selector: 'app-short-straddle-strategy-form',
  standalone: true,
  imports: [
    ReactiveFormsModule,
    TextInputComponent,
    SelectInputComponent,
    SwitchInputComponent,
  ],
  providers: [InstrumentService, StrategyService],
  templateUrl: './short-straddle-strategy-form.component.html',
  styleUrl: './short-straddle-strategy-form.component.css',
})
export class ShortStraddleStrategyFormComponent {
  constructor(
    private instrumentService: InstrumentService,
    private strategyService: StrategyService
  ) {}
  form = new FormGroup({
    user: new FormControl('rajashekar670@gmail.com', Validators.required),
    id: new FormControl('', Validators.required),
    instrument: new FormControl('', Validators.required),
    quantity: new FormControl(1, Validators.min(1)),
    segment: new FormControl('OPTIONS', Validators.required),
    strategy: new FormControl('SHORT_STRADDLE', Validators.required),
    status: new FormControl('NEW', Validators.required),
    frequency: new FormControl(10, Validators.min(1)),
    label: new FormControl(''),
    paperTrade: new FormControl(false),
    pl: new FormControl(0),
    broker: new FormControl('ASQ828', Validators.required),
    expiry: new FormControl('WEEKLY', Validators.required),
    expiryDate: new FormControl('', Validators.required),
    optionType: new FormControl('SELL', Validators.required),
    additionalATMPoints: new FormControl(0),
    startTime: new FormControl('', Validators.required),
    endTime: new FormControl('', Validators.required),
    target: new FormControl(0),
    logsEnabled: new FormControl(true),

    adjustmentType: new FormControl('STRADDLE', Validators.required),
    adjustmentPercentage: new FormControl(25, Validators.required),
    callStopLossStrikePrice: new FormControl(0, Validators.required),
    putStopLossStrikePrice: new FormControl(0, Validators.required),
    callStopLossPercentage: new FormControl(40, Validators.required),
    putStopLossPercentage: new FormControl(40, Validators.required),
    callStopLossPremium: new FormControl(0, Validators.required),
    putStopLossPremium: new FormControl(0, Validators.required),
    bufferStopLossForStrikePrice: new FormControl(0, Validators.required),
    maxPL: new FormControl(0, Validators.required),
    maxDiffAdjustmentPercentage: new FormControl(50, Validators.required),
    targetPercentage: new FormControl(0, Validators.required),
    dailyStartTime: new FormControl('09:16', Validators.required),
  });

  instrumentsList: DropDown[] = [];
  statusList: DropDown[] = constants.STATUS_LIST;
  expiryList: DropDown[] = constants.EXPIRY_LIST;
  entrySelectionTypeList: DropDown[] = constants.STRANGLE_ENTRY_TYPE;
  straddleAdjustmentType: DropDown[] = constants.STRADDLE_ADJUSTMENT_TYPE;
  expiryDatesList: DropDown[] = [];
  insturments: InstrumentData[] = [];


  ngOnInit(): void {
    this.getAllInstruments();
  }

  getAllInstruments() {
    this.instrumentService.getAllInstruments().subscribe((data) => {
      this.instrumentsList = data.map((item, index) => {
        if (index == 0) {
          this.form.setControl(
            'instrument',
            new FormControl(item.name, Validators.required)
          );
          return new DropDown(item.name, item.name, true);
        }
        return new DropDown(item.name, item.name);
      });
      this.insturments = data;
      this.updateExpiryDates(this.form.value.instrument);
    });
  }

  updateExpiryDates(instrumentName: string | null | undefined) {
    this.insturments
      .filter((instrument) => instrument.name == instrumentName)
      .map((instrument) => {
        this.expiryDatesList = instrument.expiryDates.map(
          (expiryDate, index) => {
            if (index == 0) {
              this.form.setControl(
                'expiryDate',
                new FormControl(expiryDate.expiryDate, Validators.required)
              );
              return new DropDown(
                expiryDate.expiryDate,
                expiryDate.expiryDate,
                true
              );
            }
            return new DropDown(
              expiryDate.expiryDate,
              expiryDate.expiryDate,
              false
            );
          }
        );
      });

    if (this.expiryDatesList.length == 0) {
      this.form.setControl(
        'expiryDate',
        new FormControl('', Validators.required)
      );
    }
  }

  submit() {
    this.strategyService.createStrategy(this.form.value).subscribe();
    alert(JSON.stringify(this.form.value));
  }
}
