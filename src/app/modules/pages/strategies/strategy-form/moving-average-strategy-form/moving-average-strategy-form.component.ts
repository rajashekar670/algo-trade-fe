import { Component, OnChanges } from '@angular/core';
import {
  FormControl,
  FormGroup,
  ReactiveFormsModule,
  Validators,
} from '@angular/forms';
import { TextInputComponent } from '../../../../../shared/components/controls/text-input/text-input.component';
import { SelectInputComponent } from '../../../../../shared/components/controls/select-input/select-input.component';
import { constants } from '../../../../../core/constants/constants';
import { SwitchInputComponent } from '../../../../../shared/components/controls/switch-input/switch-input.component';

@Component({
  selector: 'app-moving-average-strategy',
  standalone: true,
  imports: [
    ReactiveFormsModule,
    TextInputComponent,
    SelectInputComponent,
    SwitchInputComponent,
  ],
  templateUrl: './moving-average-strategy-form.component.html',
  styleUrl: './moving-average-strategy-form.component.css',
})
export class MovingAverageStrategyFormComponent {
  segments = constants.SEGMENTS_LIST;
  strategies = constants.STRATEGIES_LIST;
  status = constants.STATUS_LIST;
  expiry = constants.EXPIRY_LIST;
  optionType = constants.OPTION_TYPE_LIST;

  form = new FormGroup({
    id: new FormControl('', Validators.required),
    instrument: new FormControl('BANKNIFTY', Validators.required),
    quantity: new FormControl(1, Validators.min(1)),
    segment: new FormControl('OPTIONS', Validators.required),
    strategy: new FormControl('MOVING_AVERAGE', Validators.required),
    status: new FormControl('NEW', Validators.required),
    frequency: new FormControl(0),
    label: new FormControl(''),
    paperTrade: new FormControl(false),
    pl: new FormControl(0),
    broker: new FormControl('ASQ828', Validators.required),
    user: new FormControl('rajashekar670@gmail.com', Validators.required),
    expiry: new FormControl('WEEKLY', Validators.required),
    expiryDate: new FormControl('', Validators.required),
    optionType: new FormControl('BUY', Validators.required),
    additionalATMPoints: new FormControl(0),
    startTime: new FormControl('', Validators.required),
    endTime: new FormControl('', Validators.required),
    high: new FormControl(0, Validators.min(0)),
    low: new FormControl(0, Validators.min(0)),
    timeFrame: new FormControl(5, Validators.min(1)),
    period: new FormControl(5, Validators.min(1)),
    noOfEntries: new FormControl(2, Validators.min(1)),
    stopLossHits: new FormControl(0, Validators.min(0)),
    buy: new FormControl(false),
  });

  submit() {
    alert(this.form.value);
  }

  updateSegmentData(event:any) {
    alert('');
  }
}
