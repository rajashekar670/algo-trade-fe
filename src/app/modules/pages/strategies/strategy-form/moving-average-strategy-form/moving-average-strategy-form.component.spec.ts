import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MovingAverageStrategyFormComponent } from './moving-average-strategy-form.component';

describe('MovingAverageStrategyComponent', () => {
  let component: MovingAverageStrategyFormComponent;
  let fixture: ComponentFixture<MovingAverageStrategyFormComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [MovingAverageStrategyFormComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(MovingAverageStrategyFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
