import {
  AfterViewInit,
  ChangeDetectorRef,
  Component,
  EventEmitter,
  OnChanges,
  OnInit,
  SimpleChanges,
} from '@angular/core';
import {
  FormControl,
  FormGroup,
  ReactiveFormsModule,
  Validators,
} from '@angular/forms';
import { TextInputComponent } from '../../../../../shared/components/controls/text-input/text-input.component';
import { InstrumentService } from '../../../../../core/services/instrument.service';
import { SelectInputComponent } from '../../../../../shared/components/controls/select-input/select-input.component';
import { DropDown } from '../../../../../shared/types/dropdown';
import { constants } from '../../../../../core/constants/constants';
import { SwitchInputComponent } from '../../../../../shared/components/controls/switch-input/switch-input.component';
import { StrategyService } from '../../../../../core/services/strategy.service';
import { InstrumentData } from '../../../../../core/types/data/instrument-data';
import { ExpiryDateData } from '../../../../../core/types/data/expiry-date-data';
import exp from 'constants';
import { CommonModule } from '@angular/common';

@Component({
  selector: 'app-short-strangle-strategy',
  standalone: true,
  imports: [
    ReactiveFormsModule,
    TextInputComponent,
    SelectInputComponent,
    SwitchInputComponent,
    CommonModule,
  ],
  providers: [InstrumentService, StrategyService],
  templateUrl: './short-strangle-strategy-form.component.html',
  styleUrl: './short-strangle-strategy-form.component.css',
})
export class ShortStrangleStrategyFormComponent implements OnInit {
  constructor(
    private instrumentService: InstrumentService,
    private strategyService: StrategyService,
    private changeDetectionRef: ChangeDetectorRef
  ) {}

  form = new FormGroup({
    user: new FormControl('rajashekar670@gmail.com', Validators.required),
    id: new FormControl('', Validators.required),
    instrument: new FormControl('', Validators.required),
    quantity: new FormControl(1, Validators.min(1)),
    segment: new FormControl('OPTIONS', Validators.required),
    strategy: new FormControl('SHORT_STRANGLE', Validators.required),
    status: new FormControl('NEW', Validators.required),
    frequency: new FormControl(10, Validators.min(1)),
    label: new FormControl(''),
    paperTrade: new FormControl(false),
    pl: new FormControl(0),
    broker: new FormControl('ASQ828', Validators.required),
    expiry: new FormControl('WEEKLY', Validators.required),
    expiryDate: new FormControl('', Validators.required),
    optionType: new FormControl('SELL', Validators.required),
    additionalATMPoints: new FormControl(0),
    startTime: new FormControl('', Validators.required),
    endTime: new FormControl('', Validators.required),
    target: new FormControl(0),
    logsEnabled: new FormControl(true),

    entrySelectionType: new FormControl('PREMIUM', Validators.required),
    callStrikePrice: new FormControl(0),
    putStrikePrice: new FormControl(0),
    callPremium: new FormControl(0),
    putPremium: new FormControl(0),
    dailyStartTime: new FormControl('09:16', Validators.required),
    dailyEndTime: new FormControl('15:25', Validators.required),
    eodMaxDiffPercentage: new FormControl(30, Validators.required),
    adjustmentMaxDiffPercentage: new FormControl(50, Validators.required),
    minAdjustmentPercentage: new FormControl(80, Validators.required),
    straddleAdjustmentType: new FormControl('STOPLOSS', Validators.required),
    stoplossPremium: new FormControl(0, Validators.required),
    stoplossUpdateDiffPercentage: new FormControl(5, Validators.required),
    stoplossUpdatePercentage: new FormControl(10, Validators.required),
    totalPremium: new FormControl(0),
    callStopLossStrikePrice: new FormControl(0),
    putStopLossStrikePrice: new FormControl(0),
    straddleAdjustmentPercentage: new FormControl(25, Validators.required),
    targetPercentage: new FormControl(0),
  });

  instrumentsList: DropDown[] = [];
  statusList: DropDown[] = constants.STATUS_LIST;
  expiryList: DropDown[] = constants.EXPIRY_LIST;
  entrySelectionTypeList: DropDown[] = constants.STRANGLE_ENTRY_TYPE;
  straddleAdjustmentType: DropDown[] = constants.STRADDLE_ADJUSTMENT_TYPE;
  expiryDatesList: DropDown[] = [];
  insturments: InstrumentData[] = [];

  ngOnInit(): void {
    this.getAllInstruments();
  }

  getAllInstruments() {
    this.instrumentService.getAllInstruments().subscribe((data) => {
      this.instrumentsList = data.map((item, index) => {
        if (index == 0) {
          this.form.setControl(
            'instrument',
            new FormControl(item.name, Validators.required)
          );
          return new DropDown(item.name, item.name, true);
        }
        return new DropDown(item.name, item.name);
      });
      this.insturments = data;
      this.updateExpiryDates(this.form.value.instrument);
    });
  }

  updateExpiryDates(instrumentName: string | null | undefined) {
    console.log(instrumentName);
    this.insturments
      .filter((instrument) => instrument.name == instrumentName)
      .map((instrument) => {
        this.expiryDatesList = instrument.expiryDates.map(
          (expiryDate, index) => {
            if (index == 0) {
              this.form.setControl(
                'expiryDate',
                new FormControl(expiryDate.expiryDate, Validators.required)
              );
              return new DropDown(
                expiryDate.expiryDate,
                expiryDate.expiryDate,
                true
              );
            }
            return new DropDown(
              expiryDate.expiryDate,
              expiryDate.expiryDate,
              false
            );
          }
        );
      });

    if (this.expiryDatesList.length == 0) {
      this.form.setControl(
        'expiryDate',
        new FormControl('', Validators.required)
      );
    }
  }

  submit() {
    this.strategyService.createStrategy(this.form.value).subscribe();
    alert(JSON.stringify(this.form.value));
  }

}
