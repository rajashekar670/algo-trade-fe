import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ShortStrangleStrategyFormComponent } from './short-strangle-strategy-form.component';

describe('ShortStrangleStrategyComponent', () => {
  let component: ShortStrangleStrategyFormComponent;
  let fixture: ComponentFixture<ShortStrangleStrategyFormComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [ShortStrangleStrategyFormComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(ShortStrangleStrategyFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
