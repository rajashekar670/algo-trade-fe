import { AfterContentInit, Component, OnDestroy, OnInit } from '@angular/core';
import { StrategyService } from '../../../../core/services/strategy.service';
import { PagedStrategyData } from '../../../../core/types/data/paged-strategy-data';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { Subscription, interval } from 'rxjs';
import { constants } from '../../../../core/constants/constants';
import { DropDown } from '../../../../shared/types/dropdown';
import { FormsModule } from '@angular/forms';
import { StrategyData } from '../../../../core/types/data/strategy-data';
import { SchedulerService } from '../../../../core/services/schedulerService';

@Component({
  selector: 'app-strategies',
  standalone: true,
  imports: [CommonModule, RouterModule, FormsModule, CommonModule],
  providers: [StrategyService, SchedulerService],
  templateUrl: './strategy-list.component.html',
  styleUrl: './strategy-list.component.css',
})
export class StrategyListComponent implements OnInit, OnDestroy {
  constructor(
    private startegyService: StrategyService,
    private schedulerService: SchedulerService
  ) {}

  pagedStrategies?: PagedStrategyData;
  interval$?: Subscription;
  statusList: DropDown[] = constants.STATUS_LIST;
  status: string = 'RUNNING';

  ngOnDestroy(): void {
    this.interval$?.unsubscribe();
  }

  ngOnInit(): void {
    this.refreshStrategies();
    this.interval$ = interval(5000).subscribe((data) =>
      this.refreshStrategies()
    );
    if (this.statusList.filter((status) => status.key == 'All').length == 0) {
      this.statusList.push({ key: 'All', value: '' });
    }
  }

  startStrategy(id: string) {
    this.startegyService.startStrategy(id).subscribe();
  }

  refreshStrategies() {
    this.startegyService
      .getStrategies(this.status)
      .subscribe((data) => (this.pagedStrategies = data));
  }

  updateStatus(event: any) {
    this.status = event.target.value;
    this.refreshStrategies();
  }

  deleteStrategy(data: StrategyData) {
    console.log(data);
    this.startegyService.deleteStrategy(data.id, data.user.email);
  }

  stopStrategy(id: string) {
    this.schedulerService.deleteJob(id);
    console.log("Strategy job deleted : " + id);
  }
}
