import { Component, Input } from '@angular/core';
import { Strategy } from '../../../../../core/types/strategy';
import { StrategyData } from '../../../../../core/types/data/strategy-data';

@Component({
  selector: '.app-strategy',
  standalone: true,
  imports: [],
  templateUrl: './strategy.component.html',
  styleUrl: './strategy.component.css'
})
export class StrategyComponent{

  @Input() data!: StrategyData;


}
