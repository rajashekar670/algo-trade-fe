import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ShortStraddleStrategyDetailComponent } from './short-straddle-strategy-detail.component';

describe('ShortStraddleStrategyDetailComponent', () => {
  let component: ShortStraddleStrategyDetailComponent;
  let fixture: ComponentFixture<ShortStraddleStrategyDetailComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [ShortStraddleStrategyDetailComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(ShortStraddleStrategyDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
