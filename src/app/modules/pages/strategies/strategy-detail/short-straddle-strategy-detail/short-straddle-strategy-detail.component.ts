import { Component } from '@angular/core';
import { ShortStraddleData } from '../../../../../core/types/data/short-straddle-data';
import { ActivatedRoute, RouterModule } from '@angular/router';
import { StrategyService } from '../../../../../core/services/strategy.service';
import { StrategyEntryListComponent } from '../strategy-entry-list/strategy-entry-list.component';
import { StrategyComponent } from '../strategy/strategy.component';
import { CommonModule } from '@angular/common';
import { StrategyLogListComponent } from '../strategy-log-list/strategy-log-list.component';

@Component({
  selector: 'app-short-straddle-strategy-detail',
  standalone: true,
  imports: [RouterModule, StrategyEntryListComponent, StrategyComponent, CommonModule, StrategyLogListComponent],
  providers: [StrategyService],
  templateUrl: './short-straddle-strategy-detail.component.html',
  styleUrl: './short-straddle-strategy-detail.component.css'
})
export class ShortStraddleStrategyDetailComponent {

  constructor(private route:ActivatedRoute, private strategyService: StrategyService) {}

  data!: ShortStraddleData;

  ngOnInit(): void {
    this.route.params.subscribe(param => {
      console.log(param['id']);
      this.strategyService.getStrategy(param['id']).subscribe(data => this.data = data);
    })
  }
}
