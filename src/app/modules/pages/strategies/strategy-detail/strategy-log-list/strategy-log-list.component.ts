import { Component, Input } from '@angular/core';
import { StrategyLog } from '../../../../../core/types/strategy-log';
import { CommonModule } from '@angular/common';

@Component({
  selector: '.app-strategy-log-list',
  standalone: true,
  imports: [CommonModule],
  templateUrl: './strategy-log-list.component.html',
  styleUrl: './strategy-log-list.component.css'
})
export class StrategyLogListComponent {

  @Input() logs? : StrategyLog[];

}
