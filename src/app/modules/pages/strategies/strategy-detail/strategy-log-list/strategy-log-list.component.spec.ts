import { ComponentFixture, TestBed } from '@angular/core/testing';

import { StrategyLogListComponent } from './strategy-log-list.component';

describe('StrategyLogListComponent', () => {
  let component: StrategyLogListComponent;
  let fixture: ComponentFixture<StrategyLogListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [StrategyLogListComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(StrategyLogListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
