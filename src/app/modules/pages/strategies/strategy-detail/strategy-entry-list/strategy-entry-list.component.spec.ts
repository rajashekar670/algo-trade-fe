import { ComponentFixture, TestBed } from '@angular/core/testing';

import { StrategyEntryListComponent } from './strategy-entry-list.component';

describe('StrategyEntryListComponent', () => {
  let component: StrategyEntryListComponent;
  let fixture: ComponentFixture<StrategyEntryListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [StrategyEntryListComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(StrategyEntryListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
