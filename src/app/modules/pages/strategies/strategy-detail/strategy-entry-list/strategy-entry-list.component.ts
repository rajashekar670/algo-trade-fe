import { Component, Input, OnInit } from '@angular/core';
import { StrategyEntryData } from '../../../../../core/types/data/strategy-entry-data';

@Component({
  selector: '.app-strategy-entry-list',
  standalone: true,
  imports: [],
  templateUrl: './strategy-entry-list.component.html',
  styleUrl: './strategy-entry-list.component.css'
})
export class StrategyEntryListComponent {

  @Input() entries? : StrategyEntryData[];
  

}
