import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ShortStrangleStrategyDetailComponent } from './short-strangle-strategy-detail.component';

describe('ShortStrangleStrategyDetailComponent', () => {
  let component: ShortStrangleStrategyDetailComponent;
  let fixture: ComponentFixture<ShortStrangleStrategyDetailComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [ShortStrangleStrategyDetailComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(ShortStrangleStrategyDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
