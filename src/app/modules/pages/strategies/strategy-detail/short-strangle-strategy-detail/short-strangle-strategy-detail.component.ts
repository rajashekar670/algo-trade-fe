import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, RouterModule } from '@angular/router';
import { StrategyService } from '../../../../../core/services/strategy.service';
import { ShortStrangleData } from '../../../../../core/types/data/short-strangle-data';
import { StrategyEntryListComponent } from '../strategy-entry-list/strategy-entry-list.component';
import { StrategyComponent } from '../strategy/strategy.component';
import { StrategyListComponent } from '../../strategy-list/strategy-list.component';
import { CommonModule } from '@angular/common';
import { StrategyLogListComponent } from '../strategy-log-list/strategy-log-list.component';

@Component({
  selector: 'app-short-strangle-strategy-detail',
  standalone: true,
  imports: [RouterModule, StrategyEntryListComponent, StrategyComponent, CommonModule, StrategyLogListComponent],
  providers: [StrategyService],
  templateUrl: './short-strangle-strategy-detail.component.html',
  styleUrl: './short-strangle-strategy-detail.component.css',
})
export class ShortStrangleStrategyDetailComponent implements OnInit {

  constructor(private route:ActivatedRoute, private strategyService: StrategyService) {}

  data!: ShortStrangleData;

  ngOnInit(): void {
    this.route.params.subscribe(param => {
      console.log(param['id']);
      this.strategyService.getStrategy(param['id']).subscribe(data => this.data = data);
    })
  }
}
